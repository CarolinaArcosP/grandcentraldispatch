//
//  ViewController.swift
//  Tasks
//
//  Created by Bart Jacobs on 06/11/2018.
//  Copyright © 2018 Cocoacasts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var imageViews: [UIImageView]!
    
    // MARK: - Properties
    private lazy var images: [URL] = [
        URL(string: "https://cdn.cocoacasts.com/7ba5c3e7df669703cd7f0f0d4cefa5e5947126a8/1.jpg")!,
        Bundle.main.url(forResource: "2", withExtension: "jpg")!,
        URL(string: "https://cdn.cocoacasts.com/7ba5c3e7df669703cd7f0f0d4cefa5e5947126a8/3.jpg")!,
        Bundle.main.url(forResource: "4", withExtension: "jpg")!
    ]
    
    private let serialDispatchQueue = DispatchQueue(label: "Serial Dispatch Queue")
    private let concurrentDispatchQueue = DispatchQueue(label: "Concurrent Dispatch Queue",
                                                        attributes: .concurrent)

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Start \(Date())")
        
        for (index, imageView) in imageViews.enumerated() {
            // Fetch URL
            let url = images[index]
            
            DispatchQueue.global(qos: .utility).sync { [weak self] in
                // Populate Image View
                self?.loadImage(with: url, for: imageView)
            }
        }
        
        print("Finish \(Date())")
    }
    
    // MARK: - Helper Methods
    private func loadImage(with url: URL, for imageView: UIImageView) {
        // Load Data
        guard let data = try? Data(contentsOf: url) else {
            return
        }
        
        // Create Image
        let image = UIImage(data: data)
        
        DispatchQueue.main.async {
            // Update Image View
            imageView.image = image
        }
    }

}
