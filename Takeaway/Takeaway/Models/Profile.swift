//
//  Profile.swift
//  Takeaway
//
//  Created by Bart Jacobs on 11/05/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import Foundation

struct Profile: Codable {
    
    // MARK: - Properties
    
    let first: String
    let last: String
    
    // MARK: -
    
    let balance: Float
    
}
