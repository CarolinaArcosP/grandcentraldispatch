//
//  Order.swift
//  Takeaway
//
//  Created by Bart Jacobs on 11/05/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import Foundation

struct Order: Codable {
    
    // MARK: - Properties
    
    let date: Date
    let price: Double
    let contents: String
    
}
