//
//  ProfileViewController.swift
//  Takeaway
//
//  Created by Bart Jacobs on 11/05/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Propertieshttps://docs.google.com/document/d/1bn0c43hGEoaJEe-G3Y1jkR6-xNCOAtpa/edit#heading=h.3fwokq0
    
    private let networkManager = NetworkManager()
    private var profile: Profile?
    private var orders: [Order] = []
    
    // MARK: -
    
    private let dateFormatter: DateFormatter = {
        // Initialize Date Formatter
        let dateFormatter = DateFormatter()
        
        // Configure Date Formatter
        dateFormatter.dateFormat = "E, MMM d, yyyy"
        
        return dateFormatter
    }()
    
    // MARK: - Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Set Title
        title = "Profile"
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Fetch Data
        fetchData()
    }
    
    // MARK: - Helper Methods

    private func fetchData() {
        tableView.isHidden = true
        activityIndicatorView.startAnimating()
        
        let dispatchGroup = DispatchGroup()
        
        // Fetch Profile
        dispatchGroup.enter()
        networkManager.fetchProfile { [weak self] (result) in
            switch result {
            case .success(let profile):
                self?.profile = profile
            case .failure(let error):
                print(error)
            }
            
            dispatchGroup.leave()
        }
        
        // Fetch Orders
        dispatchGroup.enter()
        networkManager.fetchOrders { [weak self] (result) in
            switch result {
            case .success(let orders):
                self?.orders = orders
            case .failure(let error):
                print(error)
            }
            
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            self.tableView.reloadData()
            self.tableView.isHidden = false
            self.activityIndicatorView.stopAnimating()
        }
    }
    
}

extension ProfileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numberOfSections: Int = profile == nil ? 0 : 1
        return numberOfSections + (orders.isEmpty ? 0 : 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profile != nil, section == 0 {
            return 1
        } else {
            return orders.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let profile = profile, indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.reuseIdentifier, for: indexPath) as? ProfileTableViewCell else {
                fatalError("Unable to Dequeue Profile Table View Cell")
            }
            
            // Configure Cell
            cell.nameLabel.text = "\(profile.first) \(profile.last)"
            cell.balanceLabel.text = "$\(profile.balance)"
            
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderTableViewCell.reuseIdentifier, for: indexPath) as? OrderTableViewCell else {
                fatalError("Unable to Dequeue Order Table View Cell")
            }
            
            // Fetch Order
            let order = orders[indexPath.row]
            
            // Configure Cell
            cell.contentsLabel.text = order.contents
            cell.priceLabel.text = "$\(order.price)"
            cell.dateLabel.text = dateFormatter.string(from: order.date)
            
            return cell
        }
    }
    
}
