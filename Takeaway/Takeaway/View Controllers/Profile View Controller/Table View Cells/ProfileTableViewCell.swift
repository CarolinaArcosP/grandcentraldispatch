//
//  ProfileTableViewCell.swift
//  Takeaway
//
//  Created by Bart Jacobs on 11/05/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    // MARK: - Type Properties
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    // MARK: - Properties
    
    @IBOutlet var nameLabel: UILabel! {
        didSet {
            // Configure Name Label
            nameLabel.textAlignment = .center
            nameLabel.font = UIFont.systemFont(ofSize: 15.0, weight: .light)
        }
    }
    
    @IBOutlet var balanceLabel: UILabel! {
        didSet {
            // Configure Balance Label
            balanceLabel.textAlignment = .center
            balanceLabel.font = UIFont.systemFont(ofSize: 13.0, weight: .bold)
        }
    }

}
