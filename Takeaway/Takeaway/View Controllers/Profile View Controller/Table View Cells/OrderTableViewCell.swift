//
//  OrderTableViewCell.swift
//  Takeaway
//
//  Created by Bart Jacobs on 11/05/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    // MARK: - Type Properties
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    // MARK: - Properties
    
    @IBOutlet var contentsLabel: UILabel! {
        didSet {
            // Configure Contents Label
            contentsLabel.textAlignment = .left
            contentsLabel.font = UIFont.systemFont(ofSize: 15.0, weight: .light)
        }
    }
    
    @IBOutlet var priceLabel: UILabel! {
        didSet {
            // Configure Price Label
            priceLabel.textAlignment = .left
            priceLabel.font = UIFont.systemFont(ofSize: 13.0, weight: .bold)
        }
    }

    @IBOutlet var dateLabel: UILabel! {
        didSet {
            // Configure Date Label
            dateLabel.textAlignment = .right
            dateLabel.font = UIFont.systemFont(ofSize: 13.0, weight: .bold)
        }
    }

}
