//
//  HomeViewController.swift
//  Takeaway
//
//  Created by Bart Jacobs on 11/05/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - Initialization

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Set Title
        title = "Home"
    }
    
}
