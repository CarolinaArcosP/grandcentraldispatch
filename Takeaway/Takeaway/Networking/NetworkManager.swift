//
//  NetworkManager.swift
//  Takeaway
//
//  Created by Bart Jacobs on 11/05/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import Foundation

final class NetworkManager {
    
    // MARK: - Profile
    
    enum ProfileResult {
        case success(Profile)
        case failure(ProfileError)
    }
    
    enum ProfileError {
        case notFound
        case invalidData
    }

    func fetchProfile(_ completion: @escaping (ProfileResult) -> Void) {
        URLSession.shared.dataTask(with: URL.profile) { (data, _, error) in
            sleep(2)
            
            if let data = data {
                do {
                    // Decode Data
                    let profile = try JSONDecoder().decode(Profile.self, from: data)
                    
                    // Invoke Handler
                    DispatchQueue.main.async {
                        completion(.success(profile))
                    }

                } catch {
                    // Invoke Handler
                    DispatchQueue.main.async {
                        completion(.failure(.invalidData))
                    }
                }
            } else {
                // Invoke Handler
                DispatchQueue.main.async {
                    completion(.failure(.notFound))
                }
            }
        }.resume()
    }
    
    // MARK: - Profile
    
    enum OrdersResult {
        case success([Order])
        case failure(OrdersError)
    }
    
    enum OrdersError {
        case notFound
        case invalidData
    }
    
    func fetchOrders(_ completion: @escaping (OrdersResult) -> Void) {
        URLSession.shared.dataTask(with: URL.orders) { (data, _, error) in
            sleep(1)
            
            if let data = data {
                // Create JSON Decoder
                let decoder = JSONDecoder()
                
                // Configure JSON Decoder
                decoder.dateDecodingStrategy = .iso8601
                
                do {
                    // Decode Data
                    let unsortedOrders = try decoder.decode([Order].self, from: data)
                    
                    // Sort Orders
                    let sortedOrders = unsortedOrders.sorted(by: { $0.date > $1.date })
                    
                    // Invoke Handler
                    DispatchQueue.main.async {
                        completion(.success(sortedOrders))
                    }
                    
                } catch {
                    // Invoke Handler
                    DispatchQueue.main.async {
                        completion(.failure(.invalidData))
                    }
                }
            } else {
                // Invoke Handler
                DispatchQueue.main.async {
                    completion(.failure(.notFound))
                }
            }
        }.resume()
    }

}

fileprivate extension URL {
    
    static var base: URL {
        return URL(string: "https://cocoacasts.s3.amazonaws.com/15eb4a8ed546df91dd5c1c50cd6250da01503242")!
    }
    
    static var profile: URL {
        return base.appendingPathComponent("profile.json")
    }
    
    static var orders: URL {
        return base.appendingPathComponent("orders.json")
    }

}
