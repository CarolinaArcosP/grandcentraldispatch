import Foundation

let group = DispatchGroup()


print("START")
group.enter()
let dataTask0 = URLSession.shared.dataTask(with: URL(string: "https://cocoacasts.com")!) { _,_,_ in
    print("DATA TASK 0 COMPLETED")
    
    group.leave()
}

dataTask0.resume()

group.enter()
let dataTask1 = URLSession.shared.dataTask(with: URL(string: "https://apple.com")!) { _,_,_ in
    print("DATA TASK 1 COMPLETED")
    
    group.leave()
}

dataTask1.resume()

print("END 1")
group.notify(queue: .main) {
    print("NOTIFIED")
}

let result = group.wait(timeout: .now() + 0.2)
print("value \(result)")

print("END 2")
