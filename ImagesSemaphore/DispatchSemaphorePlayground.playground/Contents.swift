import Foundation

//print("START")
//
//let semaphore = DispatchSemaphore(value: 0) //1. = 0
//
//print("SCHEDULE DATA TASK 0")
//
//let dataTask0 = URLSession.shared.dataTask(with: URL(string: "https://cocoacasts.com")!) { _,_,_ in
//    print("DATA TASK 0 COMPLETED")
//
//    semaphore.signal() // 3. = 0
//}
//
//dataTask0.resume()
//semaphore.wait() // 2. = -1 -> thread blocked
//
//print("SCHEDULE DATA TASK 1")
//
//let dataTask1 = URLSession.shared.dataTask(with: URL(string: "https://apple.com")!) { _,_,_ in
//    print("DATA TASK 1 COMPLETED")
//
//    semaphore.signal() // 5. = 0
//}
//
//dataTask1.resume()
//semaphore.wait() // 4. = -1 -> thread blocked
//
//print("END")

// MARK: - MIMICK DISPATCH GROUP OP 1 --> not the expected result
//print("START")
//
//let semaphore = DispatchSemaphore(value: 1) //1. = 1
//
//print("SCHEDULE DATA TASK 0")
//
//let dataTask0 = URLSession.shared.dataTask(with: URL(string: "https://cocoacasts.com")!) { _,_,_ in
//    print("DATA TASK 0 COMPLETED")
//
//    semaphore.signal() // 4. = 0
//}
//
//dataTask0.resume()
//semaphore.wait() // 2. = 0 -> thread not blocked
//
//print("SCHEDULE DATA TASK 1")
//
//let dataTask1 = URLSession.shared.dataTask(with: URL(string: "https://apple.com")!) { _,_,_ in
//    print("DATA TASK 1 COMPLETED")
//
//    semaphore.signal() // 4. = 0
//}
//
//dataTask1.resume()
//semaphore.wait() // 3. = -1 -> thread blocked
//
//print("END")


// MARK: - MIMICK DISPATCH GROUP OP 2 --> expected result
//print("START")
//
//let semaphore = DispatchSemaphore(value: 0) //1. = 0
//
//print("SCHEDULE DATA TASK 0")
//
//let dataTask0 = URLSession.shared.dataTask(with: URL(string: "https://cocoacasts.com")!) { _,_,_ in
//    print("DATA TASK 0 COMPLETED")
//
//    semaphore.signal() // 4. = -1/0
//}
//
//dataTask0.resume()
//
//print("SCHEDULE DATA TASK 1")
//
//let dataTask1 = URLSession.shared.dataTask(with: URL(string: "https://apple.com")!) { _,_,_ in
//    print("DATA TASK 1 COMPLETED")
//
//    semaphore.signal() // 4. = -1/0
//}
//
//dataTask1.resume()
//
//semaphore.wait() // 2. = -1 -> thread not blocked
//semaphore.wait() // 3. = -2 -> thread blocked
//
//print("END")


// MARK: - Deadlock
//print("START")
//
//let semaphore = DispatchSemaphore(value: 0)
//
//print("SCHEDULE DATA TASK 0")
//
//let dataTask0 = URLSession.shared.dataTask(with: URL(string: "https://cocoacasts.com")!) { _,_,_ in
//    print("DATA TASK 0 COMPLETED")
//
//    semaphore.signal()
//}
//
///* The execution of the thread is blocked before the task is completed, so the completion handler of the dataTask is never called and the semaphore's value is never incremeted to resume the execution od the current thread */
//semaphore.wait()
//dataTask0.resume()
//
//print("SCHEDULE DATA TASK 1")
//
//let dataTask1 = URLSession.shared.dataTask(with: URL(string: "https://apple.com")!) { _,_,_ in
//    print("DATA TASK 1 COMPLETED")
//
//    semaphore.signal()
//}
//
//dataTask1.resume()
//semaphore.wait()
//
//print("END")

// MARK: - Deadlock - Multiple threads
print("START")

let semaphore = DispatchSemaphore(value: 0)

print("SCHEDULE DATA TASK 0")

let dataTask0 = URLSession.shared.dataTask(with: URL(string: "https://cocoacasts.com")!) { _,_,_ in
    print("DATA TASK 0 COMPLETED")

    DispatchQueue.main.async {
        semaphore.signal()
    }
}

dataTask0.resume()
semaphore.wait()

print("SCHEDULE DATA TASK 1")

let dataTask1 = URLSession.shared.dataTask(with: URL(string: "https://apple.com")!) { _,_,_ in
    print("DATA TASK 1 COMPLETED")

    DispatchQueue.main.async {
        semaphore.signal()
    }
}

dataTask1.resume()
semaphore.wait()

print("END")
