//
//  ImageService.swift
//  Images
//
//  Created by Carolina Arcos on 27/07/21.
//  Copyright © 2021 Cocoacasts. All rights reserved.
//

import UIKit

class ImageService {
    // Utility: triggered by the user but is not waiting for response. Takes some time and at the end are visible to te user
    private let imageQueue = DispatchQueue(label: "Image Queue", qos: .utility, attributes: [.concurrent])
    private let semaphore = DispatchSemaphore(value: 3)
    private var counter = 0
    
    func image(with url: URL, completion: @escaping (UIImage?) -> Void) {
        imageQueue.async {
            let result = self.semaphore.wait(wallTimeout: .now() + 5.0) // should not wait in the main thread, that's why this needs to be called inside the async and not before.
            
            self.counter += 1
            let localCounter = self.counter
            
            var image: UIImage?
            
            print("START \(localCounter), TIME OUT \(result == .timedOut)")
            
            if let data = try? Data(contentsOf: url) {
                image = UIImage(data: data)
            }
            
            DispatchQueue.main.async {
                completion(image)
                let resultSignal = self.semaphore.signal()
                print("END \(localCounter), THREAD WOKEN \(resultSignal > 0)")
            }
        }
    }
}

