//
//  ImageTableViewCell.swift
//  Images
//
//  Created by Bart Jacobs on 31/07/2018.
//  Copyright © 2018 Cocoacasts. All rights reserved.
//

import UIKit

final class ImageTableViewCell: UITableViewCell {

    // MARK: - Static Properties
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    // MARK: - IBOutlets
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var thumbnailImageView: UIImageView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Properties
    private weak var fetchDataWorkItem: DispatchWorkItem?

    // MARK: - Public API
    
    func configure(title: String, url: URL?) {
        // Configure Title Label
        titleLabel.text = title
        
        // Animate Activity Indicator View
        activityIndicatorView.startAnimating()
        
        if let url = url {
            var workItem: DispatchWorkItem?
            workItem = DispatchWorkItem {
                guard !(workItem?.isCancelled ?? true) else { return }
                
                if let data = try? Data(contentsOf: url) {
                    // Initialize Image
                    let image = UIImage(data: data)
                    
                    if !(workItem?.isCancelled ?? true) {
                        DispatchQueue.main.async {
                            // Configure Thumbnail Image View
                            self.thumbnailImageView.image = image
                        }
                    }
                }
            }
            
            if let workItem = workItem {
                // Delay added to avoid download unnecesary images if the user scrolls so the work item can be cancelled before execution
                DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + 0.5, execute: workItem)
            }
            
            workItem?.notify(queue: .main) {
                workItem = nil
            }

            
            fetchDataWorkItem = workItem
        }
    }
    
    // MARK: - Overrides
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // Reset Thumnail Image View
        thumbnailImageView.image = nil
        fetchDataWorkItem?.cancel()
    }
    
}
