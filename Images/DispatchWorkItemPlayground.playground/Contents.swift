import Foundation

//print("before")
//let workItem = DispatchWorkItem {
////    sleep(5)
//    let data = try! Data(contentsOf: URL(string: "https://cdn.cocoacasts.com/7ba5c3e7df669703cd7f0f0d4cefa5e5947126a8/1.jpg")!)
//    print(data.count)
//}
//
//DispatchQueue.global().async(execute: workItem)
//workItem.wait(timeout: .now() + 1.0)
//print("after")


let dispatchQueue = DispatchQueue.global(qos: .userInitiated)

dispatchQueue.async {
    print("some work")
}

let workItem = DispatchWorkItem(qos: .utility, flags: [.inheritQoS], block: {
    print("some work")
})
